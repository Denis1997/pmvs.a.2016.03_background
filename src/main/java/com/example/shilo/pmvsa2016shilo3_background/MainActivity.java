package com.example.shilo.pmvsa2016shilo3_background;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button)findViewById(R.id.changButton);
        button.setOnClickListener(this);
        view = (View)findViewById(R.id.activity_main);
        view.setBackgroundColor(getResources().getColor(R.color.yellow));
    }

    public void onClick(View v) {

        final CharSequence[] items = {
                getString(R.string.green), getString(R.string.red), getString(R.string.yellow),
                getString(R.string.image)
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Make your selection");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item)
                {
                    case 0:
                        view.setBackgroundColor(getResources().getColor(R.color.green));
                        break;
                    case 1:
                        view.setBackgroundColor(getResources().getColor(R.color.red));
                        break;
                    case 2:
                        view.setBackgroundColor(getResources().getColor(R.color.yellow));
                        break;
                    case 3:
                        view.setBackgroundResource(R.drawable.meface);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
